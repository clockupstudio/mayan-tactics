﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class SimpleEnemyMovementTests
    {
        [UnityTest]
        public IEnumerator UpAndDown_UpAndDown()
        {
            var enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("EnemyPattern1"));
            var movement = enemy.GetComponent<SimpleEnemyMovement>();
            movement.MovePattern = MovePattern.UpDown;
            var currentPosition = movement.transform.position;

            yield return new WaitForSeconds(0.1f);
            Assert.AreEqual(Vector3.zero, movement.transform.position - currentPosition);

            movement.Move();
            Assert.AreEqual(Vector3.up, movement.transform.position - currentPosition);

            movement.Move();
            Assert.AreEqual(2 * Vector3.up, movement.transform.position - currentPosition);

            movement.Move();
            // move down
            Assert.AreEqual(Vector3.up, movement.transform.position - currentPosition);

            movement.Move();
            // move down
            Assert.AreEqual(Vector3.zero, movement.transform.position - currentPosition);

            movement.Move();
            // make sure it's move up again.
            Assert.AreEqual(Vector3.up, movement.transform.position - currentPosition);
        }

        [UnityTest]
        public IEnumerator MoveLeftAndRight()
        {
            var enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("EnemyPattern1"));
            var movement = enemy.GetComponent<SimpleEnemyMovement>();
            movement.MovePattern = MovePattern.LeftRight;
            var currentPosition = movement.transform.position;

            yield return new WaitForSeconds(0.1f);
            Assert.AreEqual(Vector3.zero, movement.transform.position - currentPosition);

            movement.Move();
            Assert.AreEqual(Vector3.left, movement.transform.position - currentPosition, "move left");

            movement.Move();
            Assert.AreEqual(2 * Vector3.left, movement.transform.position - currentPosition, "move left again");

            movement.Move();
            // move right
            Assert.AreEqual(Vector3.left, movement.transform.position - currentPosition, "move right");

            movement.Move();
            // move right
            Assert.AreEqual(Vector3.zero, movement.transform.position - currentPosition, "move right again");

            movement.Move();
            // make sure it's move left again.
            Assert.AreEqual(Vector3.left, movement.transform.position - currentPosition, "move left after move right");
        }
    }
}