﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class GameManagerTests
    {
        private GameController _controller;

        private GameObject _player;
        private GameObject _manager;
        private GameObject _enemy;


        [SetUp]
        public void SetUp()
        {
            _player = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Player"));
            _enemy = MonoBehaviour.Instantiate(Resources.Load<GameObject>("EnemyPattern1"));
            _manager = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Manager"));

            _controller = _manager.GetComponent<GameController>();
            _controller.TurnStorage = ScriptableObject.CreateInstance<TurnStorage>();
            _controller.PlayerMovement = _player.GetComponent<PlayerMovement>();
            _controller.PlayerPosition = _player.GetComponent<Position>();
            _controller.Enemies = new[] {_enemy};
        }

        [TearDown]
        public void TearDown()
        {
            Object.Destroy(_manager);
            Object.Destroy(_player);
        }

        [UnityTest]
        public IEnumerator TestPlayerSwitchTurnAfterMove()
        {
            _controller.TurnStorage.Turn = Turn.Player;
            _controller.PlayerMove(Vector2.left);
            yield return new WaitForSeconds(0); // wait for 1 frame.
            Assert.AreEqual(Turn.Enemies, _controller.TurnStorage.Turn);
        }

        [UnityTest]
        public IEnumerator TestEnemySwitchTurnAfterMove()
        {
            _controller.TurnStorage.Turn = Turn.Enemies;
            yield return new WaitForSeconds(0); // wait for 1 frame.
            Assert.AreEqual(Turn.Player, _controller.TurnStorage.Turn);
        }
    }
}