﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class EnemyAttackTests
    {
        [Test]
        public void TestCanAttack()
        {
            var enemy = new GameObject();
            enemy.transform.position = new Vector3(21, 0);
            var comp = enemy.AddComponent<EnemyAttack>();

            // x x x
            // P E x
            // x x x
            Assert.True(comp.CanAttack(new Vector3(20, 0)), "enemy can attack player on the left side");

            // x x x
            // x E P
            // x x x
            Assert.True(comp.CanAttack(new Vector3(22, 0)), "enemy can attack player on the right side");

            // x P x
            // x E x
            // x x x
            Assert.True(comp.CanAttack(new Vector3(21, 1)), "enemy can attack player on the up side");

            // x x x
            // x E x
            // x P x
            Assert.True(comp.CanAttack(new Vector3(21, -1)), "enemy can attack player on the down side");

            // x x P
            // x E x
            // x x x
            Assert.False(comp.CanAttack(new Vector3(22, 1)), "player is out of attack range");

            // x x x
            // x E x
            // x x P
            Assert.False(comp.CanAttack(new Vector3(22, -1)), "player is out of attack range");
        }
    }
}