﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Act as transform but just getter.
public class Position : MonoBehaviour
{
    public Vector3 Value => transform.position;
}