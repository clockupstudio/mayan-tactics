﻿using UnityEngine;

[CreateAssetMenu(fileName = "TurnStorage", menuName = "Mayan Tactics/Settings/TurnStorage")]
public class TurnStorage : ScriptableObject
{
    // Represents current turn in the game.
    public Turn Turn = Turn.Player;
}