﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnPressedEvent : UnityEvent<Vector2> { }

public class PlayerInput : MonoBehaviour
{
    public KeyCode MoveUp = KeyCode.W;
    public KeyCode MoveDown = KeyCode.S;
    public KeyCode MoveLeft = KeyCode.A;
    public KeyCode MoveRight = KeyCode.D;

    public OnPressedEvent OnPressed;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(MoveUp))
        {
            OnPressed.Invoke(Vector2.up);
        }
        else if (Input.GetKeyDown(MoveDown))
        {
            OnPressed.Invoke(Vector2.down);
        }
        else if (Input.GetKeyDown(MoveLeft))
        {
            OnPressed.Invoke(Vector2.left);
        }
        else if (Input.GetKeyDown(MoveRight))
        {
            OnPressed.Invoke(Vector2.right);
        }
    }
}
