﻿using System;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    public AudioClip attackSound;

    private AudioSource _audioSource;
    private Animator _animator;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _animator = GetComponent<Animator>();
    }

    // Detect attack area from player position.
    public bool CanAttack(Vector3 pos)
    {
        return Math.Abs(Distance(transform.position, pos) - 1f) < 0.01;
    }

    public void Attack()
    {
        // NOTE(wingyplus): this might be play an enemy animation?.
        // NOTE(wingyplus): how do we tell game manager that player got
        // attack by enemy.
        _animator.SetBool("isAttack", true);
        Debug.Log(_animator.GetBool("isAttack"));
        _audioSource.clip = attackSound;
        _audioSource.Play();
        
    }

    private static double Distance(Vector2 v1, Vector2 v2)
    {
        return Math.Sqrt(Math.Pow(v1.y - v2.y, 2) + Math.Pow(v1.x - v2.x, 2));
    }
}