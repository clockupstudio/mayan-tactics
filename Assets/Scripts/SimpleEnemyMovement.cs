﻿using UnityEngine;

public enum MovePattern
{
    UpDown,
    LeftRight,
}

public class SimpleEnemyMovement : MonoBehaviour
{
    // Number of move before turn around.
    public short NMoveBeforeTurnAround = 2;
    public MovePattern MovePattern;

    private short _track;
    private Vector3 _dir;

    private Animator _animator;

    void Start()
    {
        _animator = GetComponent<Animator>();
        
        switch (MovePattern)
        {
            case MovePattern.UpDown:
                _dir = Vector3.up;
                
                break;
            case MovePattern.LeftRight:
                _dir = Vector3.left;
                break;
        }

        ResetTrack();
        RenderSight();
    }

    public void Move()
    {
        ResetSight();
        Animate();
        IncreaseTrack();
        transform.position += _dir;
        
        //Should be extracted to function `isFlip()` to custom the flipping rules
        if (_track == NMoveBeforeTurnAround)
        {
            FlipDirection();
            ResetTrack();
        }

        RenderSight();
    }

    private void ResetSight()
    {
        foreach (Tile tile in FindObjectsOfType<Tile>())
        {
            if (tile.transform.position.x == transform.position.x &&
                tile.transform.position.y == transform.position.y + _dir.y)
            {
                tile.Reset();
            }
        }
    }

    private void RenderSight()
    {
        foreach (Tile tile in FindObjectsOfType<Tile>())
        {
            if (tile.transform.position.x == transform.position.x &&
                tile.transform.position.y == transform.position.y + _dir.y)
            {
                tile.RenderSight();
            }
        }
    }

    private void Animate()
    {
        _animator.SetFloat("Horizontal", _dir.x);
        _animator.SetFloat("Vertical", _dir.y);
    }

    private void IncreaseTrack()
    {
        _track++;
    }

    private void ResetTrack()
    {
        _track = 0;
    }

    private void FlipDirection()
    {
        switch (MovePattern)
        {
            case MovePattern.UpDown:
                if (_dir == Vector3.up)
                {
                    _dir = Vector3.down;
                    return;
                }

                _dir = Vector3.up;
                
                break;

            case MovePattern.LeftRight:
                if (_dir == Vector3.left)
                {
                    _dir = Vector3.right;
                    return;
                }

                _dir = Vector3.left;
                break;
        }
    }
}