﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMovement : MonoBehaviour
{
    public AudioClip moveSound;

    private AudioSource _audioSource;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void Move(Vector2 dir)
    {
        _audioSource.clip = moveSound;
        _audioSource.Play();
        
        var pos = transform.position;
        transform.position = new Vector3(pos.x + dir.x, pos.y + dir.y);
    }


}