﻿using UnityEngine;

public class Tile : MonoBehaviour
{
    public Color highlightColor;
    private SpriteRenderer _renderer;

    private void Start()
    {
        _renderer = GetComponent<SpriteRenderer>();
    }

    public void Highlight()
    {
        _renderer.color = highlightColor;
    }

    public void Reset()
    {
        _renderer.color = Color.white;
    }

    public void RenderSight()
    {
        _renderer.color = Color.yellow;
    }
}
