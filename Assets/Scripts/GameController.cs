﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    #region Settings

    public TurnStorage TurnStorage;
    public PlayerMovement PlayerMovement;
    public Position PlayerPosition;

    public GameObject[] Enemies;

    #endregion

    private Vector3 _playerReceivedInput;

    // Update is called once per frame
    void Update()
    {
        switch (TurnStorage.Turn)
        {
            case Turn.Player:
                if (_playerReceivedInput == Vector3.zero)
                {
                    return;
                }

                PlayerMovement.Move(_playerReceivedInput);
                _playerReceivedInput = Vector3.zero;
                ChangeTurn();
                break;

            case Turn.Enemies:
                foreach (var enemy in Enemies)
                {
                    enemy.GetComponent<SimpleEnemyMovement>().Move();
                    if (enemy.GetComponent<EnemyAttack>().CanAttack(PlayerPosition.Value))
                    {
                        enemy.GetComponent<EnemyAttack>().Attack();
                        StartCoroutine(EnterGameOverScene());
                        
                        //NOTE: Player player dead animation here.
                        
                        Debug.Log("You lose.");
                    }
                }

                ChangeTurn();
                break;
        }
    }

    private void ChangeTurn()
    {
        switch (TurnStorage.Turn)
        {
            case Turn.Player:
                TurnStorage.Turn = Turn.Enemies;
                break;
            case Turn.Enemies:
                TurnStorage.Turn = Turn.Player;
                break;
        }
    }

    // Player move depending on input.
    public void PlayerMove(Vector2 dir)
    {
        _playerReceivedInput = dir;
    }

    private IEnumerator EnterGameOverScene()
    {
        yield return new WaitForSeconds(.5f);
        SceneManager.LoadScene("GameOverScene");
    }
}
